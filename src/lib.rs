//! This library allows you to derive the `From` trait for enums. It is used
//! to automatically generate `std::convert::From` implementations for enum
//! variants containing only one field. It currently works for tuple or struct
//! variants. Here is a small example:
//!
//! ```
//! use auto_from::From;
//!
//! #[derive(Debug, PartialEq, From)]
//! enum Foo {
//!     Int(i32),
//!     Long { value: i64 },
//! }
//!
//! assert_eq!(Foo::Int(24), Foo::from(24i32));
//! assert_eq!(Foo::Long{ value: 24 }, Foo::from(24i64));
//! ```
//!
//! This should be most useful when constructing error enums that require
//! writing many `From` implementations, or using many
//! `.map_error(|e| MyError::Variant(e))` calls across the code. This crate
//! just simplifies the process.

extern crate proc_macro;

use quote::quote;
use syn::parse_macro_input;

#[proc_macro_derive(From, attributes(auto_from))]
pub fn derive_from(item: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let item_enum = parse_macro_input!(item as syn::ItemEnum);

    let variants: Vec<_> = item_enum
        .variants
        .iter()
        .map(|variant| build_from_variant(variant, &item_enum.ident))
        .collect();

    let syntax = quote! {
        #(#variants)*
    };

    syntax.into()
}

fn build_from_variant(
    variant: &syn::Variant,
    enum_type: &syn::Ident,
) -> Option<proc_macro2::TokenStream> {
    for attr in &variant.attrs {
        match attr.parse_meta() {
            Ok(syn::Meta::List(list)) => {
                if list.ident == "auto_from" {
                    if let Some(nested) = list.nested.first() {
                        match nested.value() {
                            syn::NestedMeta::Meta(syn::Meta::Word(ident)) => {
                                if ident == "skip" {
                                    return None;
                                }
                                return Some(quote! {
                                    compile_error!(concat!("Unknown name in #[auto_from] attr: ", stringify!(#ident)));
                                });
                            }
                            _ => {
                                return Some(quote! {
                                    compile_error!("Unknown structure of #[auto_from] attr");
                                });
                            }
                        }
                    }
                }
            }
            _ => {}
        }
    }

    let field = match &variant.fields {
        syn::Fields::Named(named) => match named.named.first() {
            Some(ref pair) if named.named.len() == 1 => pair.value().clone(),
            _ => return None,
        },
        syn::Fields::Unnamed(unnamed) => match unnamed.unnamed.first() {
            Some(ref pair) if unnamed.unnamed.len() == 1 => pair.value().clone(),
            _ => return None,
        },
        syn::Fields::Unit => return None,
    };

    Some(build_from_field(&field, &variant.ident, enum_type))
}

fn build_from_field(
    field: &syn::Field,
    variant_name: &syn::Ident,
    enum_type: &syn::Ident,
) -> proc_macro2::TokenStream {
    let from_type = &field.ty;

    let variant = match &field.ident {
        Some(ident) => quote! {
            { #ident: x }
        },
        None => quote! {
            (x)
        },
    };

    quote! {
        impl From<#from_type> for #enum_type {
            fn from(x: #from_type) -> Self {
                #enum_type::#variant_name #variant
            }
        }
    }
}
